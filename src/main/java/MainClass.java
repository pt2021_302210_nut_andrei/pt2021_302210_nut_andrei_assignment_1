import controller.Controller;
import model.Model;
import view.View;

public class MainClass {

    public static void main(String[] args){
        View x = new View();
        Model mo = new Model();
        Controller c = new Controller(mo,x);
        x.setVisible(true);

    }
}
