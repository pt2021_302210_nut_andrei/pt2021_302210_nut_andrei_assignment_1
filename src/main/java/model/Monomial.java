package model;

public class Monomial implements Comparable {

    private double coeficient;
    private int power;

    public Monomial(double coeficient, int power){
        this.coeficient = coeficient;
        this.power = power;
    }

    public double getCoeficient(){
        return this.coeficient;
    }
    public int getPower(){
        return this.power;
    }

    public void setCoeficient(double coeficient){
        this.coeficient = coeficient;
    }
    public void setPower(int power){
        this.power = power;
    }

    public String toString(){
        String rez = "";
        if(this.coeficient < 0){
            rez += this.coeficient;
        }
        else{
            rez += "+" + this.coeficient;
        }
        rez += "*X^" + this.power;
        return rez;
    }

    @Override
    public int compareTo(Object o) {
        return ((Monomial)o).power - this.power;
    }
}
