package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model {

    public Polynomial addPolynomes(Polynomial firstPolynome, Polynomial secondPolynome){
        Polynomial rez = new Polynomial();
        for(Monomial x : firstPolynome.getPolynom()){
            rez.addMonome(x);
        }
        for(Monomial y : secondPolynome.getPolynom()){
            rez.addMonome(y);
        }
        Collections.sort(rez.getPolynom());
        rez = this.removeZeros(rez);
        return rez;
    }

    public Polynomial substractPolynomes(Polynomial firstPolynome, Polynomial secondPolynome){
        Polynomial rez = new Polynomial();
        for(Monomial x : firstPolynome.getPolynom()){
            rez.addMonome(x);
        }
        for(Monomial x : secondPolynome.getPolynom()){
            Monomial aux = x;
            aux.setCoeficient(-x.getCoeficient());
            rez.addMonome(aux);
        }
        Collections.sort(rez.getPolynom());
        rez = this.removeZeros(rez);
        return rez;
    }

    public Polynomial multiplyPolynomes(Polynomial firstPolynome, Polynomial secondPolynome){
        Polynomial rez = new Polynomial();
        for(Monomial x : firstPolynome.getPolynom()){
            for(Monomial y : secondPolynome.getPolynom()){
                Monomial n = new Monomial(x.getCoeficient() * y.getCoeficient(),x.getPower() + y.getPower());
                rez.addMonome(n);
            }
        }
        Collections.sort(rez.getPolynom());
        rez = this.removeZeros(rez);
        return rez;
    }

    public Polynomial derivatePolynome(Polynomial firstPolynome){
        Polynomial rez = new Polynomial();
        for(Monomial x : firstPolynome.getPolynom()){
            if(x.getPower() != 0){
                int newPower = x.getPower() - 1;
                double newCoeficient = x.getCoeficient() * x.getPower();
                rez.addMonome(new Monomial(newCoeficient, newPower));
            }
        }
        Collections.sort(rez.getPolynom());
        rez = this.removeZeros(rez);
        return rez;
    }

    public Polynomial integratePolynome(Polynomial firstPolynome){
        Polynomial rez = new Polynomial();
        for(Monomial x : firstPolynome.getPolynom()){
            int newPower = x.getPower()+1;
            double newCoeficient = x.getCoeficient()/newPower;
            rez.addMonome(new Monomial(newCoeficient,newPower));
        }
        Collections.sort(rez.getPolynom());
        rez = this.removeZeros(rez);
        return rez;
    }

    public Monomial divideMonomes(Monomial firstMonome, Monomial secondMonome){
        return new Monomial(firstMonome.getCoeficient()/secondMonome.getCoeficient(), firstMonome.getPower() - secondMonome.getPower());
    }

    public ArrayList<Polynomial> dividePolynome(Polynomial firstPolynome, Polynomial secondPolynome){
        Polynomial cat = new Polynomial();
        Polynomial rest = new Polynomial();
        ArrayList<Polynomial> rez = new ArrayList<Polynomial>();
        for(Monomial x : firstPolynome.getPolynom()){
            rest.addMonome(new Monomial(x.getCoeficient(),x.getPower()));
        }
        Monomial P = new Monomial(rest.getPolynom().get(0).getCoeficient(),rest.getPolynom().get(0).getPower());
        Monomial Q = new Monomial(secondPolynome.getPolynom().get(0).getCoeficient(),secondPolynome.getPolynom().get(0).getPower());
        while(P.getPower() >= Q.getPower() && !rest.getPolynom().isEmpty()){
            Polynomial aux = new Polynomial();
            cat.addMonome(this.divideMonomes(P,Q));
            aux.addMonome(this.divideMonomes(P,Q));
            rest = this.substractPolynomes(rest,this.multiplyPolynomes(secondPolynome,aux));
            P = new Monomial(rest.getPolynom().get(0).getCoeficient(),rest.getPolynom().get(0).getPower());
        }
        cat = this.removeZeros(cat);
        rest = this.removeZeros(rest);
        rez.add(cat);
        rez.add(rest);
        return rez;
    }

    public Polynomial removeZeros(Polynomial x){
        Polynomial rez = new Polynomial();
        for(Monomial i : x.getPolynom()){
            if(i.getCoeficient() != 0){
                rez.addMonome(i);
            }
        }
        return rez;
    }

    public boolean rightInput(String s){
        int monomeNb = 0;
        int matchesNb = 0;
        Matcher m = Pattern.compile("[+-][0-9]*\\*[X|x]\\^[0-9]*").matcher(s);
        while(m.find()){
            matchesNb++;
        }
        for(int i = 0 ; i < s.length() ; i++){
            if(s.charAt(i) == 'x' || s.charAt(i) == 'X'){
                monomeNb++;
            }
        }
        if(monomeNb == matchesNb && monomeNb != 0){
            return true;
        }
        return false;
    }

    public Polynomial fromStringToPolynome(String s){
        Polynomial rez = new Polynomial();
        Matcher m = Pattern.compile("([+-][0-9]*)\\*[X|x]\\^([0-9]*)").matcher(s);
        while(m.find()){
                int monC = Integer.parseInt(m.group(1));
                int monP = Integer.parseInt(m.group(2));
                Monomial mon = new Monomial(monC,monP);
                rez.addMonome(mon);
            }
        return rez;
    }

}
