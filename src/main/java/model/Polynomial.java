package model;

import java.util.ArrayList;

public class Polynomial {

    private ArrayList<Monomial> polynom;

    public Polynomial(){
        this.polynom = new ArrayList<Monomial>();
    }

    public ArrayList<Monomial> getPolynom(){
        return this.polynom;
    }

    public void addMonome(Monomial x){
        boolean inPolynom = false;
        for(Monomial i : this.polynom){
            if(i.getPower() == x.getPower()){
                i.setCoeficient(i.getCoeficient() + x.getCoeficient());
                inPolynom = true;
            }
        }
        if(!inPolynom){
            this.polynom.add(x);
        }
    }
    public String toString(){
        String rez = "";
        for(Monomial x : polynom){
            rez += x.toString();
        }
        return rez;
    }
}
