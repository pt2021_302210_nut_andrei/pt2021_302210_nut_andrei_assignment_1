package controller;

import model.Polynomial;
import view.View;
import model.Model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Controller {

    private Model model;
    private View view;

    public Controller(Model model, View view){
        this.model = model;
        this.view = view;

        this.view.addAditionActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(model.rightInput(view.getFirstPolynome()) && model.rightInput(view.getSecondPolynome())){
                    view.showMessage(model.addPolynomes(model.fromStringToPolynome(view.getFirstPolynome()),model.fromStringToPolynome(view.getSecondPolynome())).toString());
                }
                else{
                    view.showMessage("Bad input! Please use the following pattern: sign coeficient *X^power");
                }
            }
        });

        this.view.addSubstrActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(model.rightInput(view.getFirstPolynome()) && model.rightInput(view.getSecondPolynome())){
                    view.showMessage(model.substractPolynomes(model.fromStringToPolynome(view.getFirstPolynome()),model.fromStringToPolynome(view.getSecondPolynome())).toString());
                }
                else{
                    view.showMessage("Bad input! Please use the following pattern: sign coeficient *X^power");
                }
            }
        });

        this.view.addMultiplyActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e){
                if(model.rightInput(view.getFirstPolynome()) && model.rightInput(view.getSecondPolynome())){
                    view.showMessage(model.multiplyPolynomes(model.fromStringToPolynome(view.getFirstPolynome()),model.fromStringToPolynome(view.getSecondPolynome())).toString());
                }
                else{
                    view.showMessage("Bad input! Please use the following pattern: sign coeficient *X^power");
                }
            }
        });

        this.view.addDerivActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                if(model.rightInput(view.getFirstPolynome())){
                    view.showMessage(model.derivatePolynome(model.fromStringToPolynome(view.getFirstPolynome())).toString());
                }
                else{
                    view.showMessage("Bad input! Please use the following pattern: sign coeficient *X^power");
                }
            }
        });

        this.view.addIntegrateActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                if(model.rightInput(view.getFirstPolynome())){
                    view.showMessage(model.integratePolynome(model.fromStringToPolynome(view.getFirstPolynome())).toString());
                }
                else{
                    view.showMessage("Bad input! Please use the following pattern: sign coeficient *X^power");
                }
            }
        });

        this.view.addDivideActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(model.rightInput(view.getFirstPolynome()) && model.rightInput(view.getSecondPolynome())){
                    if(view.getFirstPolynome().equals(view.getSecondPolynome())){
                        view.showMessage("1","0");
                    }
                    else{
                        ArrayList<Polynomial> results = model.dividePolynome(model.fromStringToPolynome(view.getFirstPolynome()),model.fromStringToPolynome(view.getSecondPolynome()));
                        view.showMessage(results.get(0).toString(),results.get(1).toString());
                    }
                }
                else{
                    view.showMessage("Bad input! Please use the following pattern: sign coeficient *X^power");
                }
            }
        });
    }

}
