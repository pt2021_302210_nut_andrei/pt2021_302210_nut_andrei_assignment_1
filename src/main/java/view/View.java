package view;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {

    private JPanel contentPane;
    private JTextField firstP;
    private JTextField secondP;
    private JLabel resultLabel;
    private JButton intBtn;
    private JButton derBtn;
    private JButton divBtn;
    private JButton mulBtn;
    private JButton substrBtn;
    private JButton addBtn;
    private JLabel secondLabel;
    private JLabel firstLabel;


    public View() {
        setTitle("Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 460, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        firstP = new JTextField();
        firstP.setFont(new Font("Tahoma", Font.PLAIN, 14));
        firstP.setBounds(75, 40, 286, 28);
        contentPane.add(firstP);
        firstP.setColumns(10);

        firstLabel = new JLabel("Enter the first polynome");
        firstLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
        firstLabel.setBounds(75, 11, 220, 18);
        contentPane.add(firstLabel);

        secondP = new JTextField();
        secondP.setFont(new Font("Tahoma", Font.PLAIN, 14));
        secondP.setColumns(10);
        secondP.setBounds(75, 122, 286, 28);
        contentPane.add(secondP);

        secondLabel = new JLabel("Enter the second polynome");
        secondLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
        secondLabel.setBounds(75, 93, 220, 18);
        contentPane.add(secondLabel);

        addBtn = new JButton("Add");
        addBtn.setBounds(75, 177, 89, 23);
        contentPane.add(addBtn);

        substrBtn = new JButton("Substr");
        substrBtn.setBounds(272, 177, 89, 23);
        contentPane.add(substrBtn);

        mulBtn = new JButton("Multiply");
        mulBtn.setBounds(75, 211, 89, 23);
        contentPane.add(mulBtn);

        divBtn = new JButton("Divide");
        divBtn.setBounds(272, 211, 89, 23);
        contentPane.add(divBtn);

        derBtn = new JButton("Derivate");
        derBtn.setBounds(75, 245, 89, 23);
        contentPane.add(derBtn);

        intBtn = new JButton("Integrate");
        intBtn.setBounds(272, 245, 89, 23);
        contentPane.add(intBtn);

    }

    public String getFirstPolynome(){
        return this.firstP.getText();
    }
    public String getSecondPolynome(){
        return this.secondP.getText();
    }
    public void addAditionActionListener(ActionListener actionListener){
        this.addBtn.addActionListener(actionListener);
    }
    public void addSubstrActionListener(ActionListener actionListener) {
        this.substrBtn.addActionListener(actionListener);
    }
    public void addMultiplyActionListener(ActionListener actionListener){
        this.mulBtn.addActionListener(actionListener);
    }
    public void addDivideActionListener(ActionListener actionListener){
        this.divBtn.addActionListener(actionListener);
    }
    public void addIntegrateActionListener(ActionListener actionListener){
        this.intBtn.addActionListener(actionListener);
    }
    public void addDerivActionListener(ActionListener actionListener){
        this.derBtn.addActionListener(actionListener);
    }

    public void showMessage(String s1, String s2){
        JOptionPane.showMessageDialog(this,"Catul:"+s1+"\nRestul:"+s2);
    }
    public void showMessage(String s1){
        JOptionPane.showMessageDialog(this,"Result:" + s1);
    }
}
