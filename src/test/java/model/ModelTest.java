package model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ModelTest {

    private Model model = new Model();

    @Test
    public void addTest(){
        Polynomial p1 = new Polynomial();
        Polynomial p2 = new Polynomial();
        Monomial m1 = new Monomial(1,1);
        Monomial m2 = new Monomial(2,1);
        p1.addMonome(m1);
        p2.addMonome(m2);
        assertTrue(model.addPolynomes(p1,p2).toString().equals("+3.0*X^1"));
    }

    @Test
    public void substractTest(){
        Polynomial p1 = new Polynomial();
        Polynomial p2 = new Polynomial();
        Monomial m1 = new Monomial(1,1);
        Monomial m2 = new Monomial(2,1);
        p1.addMonome(m1);
        p2.addMonome(m2);
        assertTrue(model.substractPolynomes(p1,p2).toString().equals("-1.0*X^1"));
    }

    @Test
    public void integrationTest(){
        Polynomial p1 = new Polynomial();
        Monomial m1 = new Monomial(3,2);
        p1.addMonome(m1);
        assertTrue(model.integratePolynome(p1).toString().equals("+1.0*X^3"));
    }

    @Test
    public void derivTest(){
        Polynomial p1 = new Polynomial();
        Monomial m1 = new Monomial(3,2);
        p1.addMonome(m1);
        assertTrue(model.derivatePolynome(p1).toString().equals("+6.0*X^1"));
    }

    @Test
    public void multiplyTest(){
        Polynomial p1 = new Polynomial();
        Polynomial p2 = new Polynomial();
        Monomial m1 = new Monomial(1,3);
        Monomial m2 = new Monomial(2,2);
        p1.addMonome(m1);
        p2.addMonome(m2);
        assertTrue(model.multiplyPolynomes(p1,p2).toString().equals("+2.0*X^5"));
    }

    @Test
    public void divisionTest(){
        Polynomial p1 = new Polynomial();
        Polynomial p2 = new Polynomial();
        p1.addMonome(new Monomial(1,3));
        p1.addMonome(new Monomial(-2,2));
        p1.addMonome(new Monomial(6,1));
        p1.addMonome(new Monomial(-5,0));
        p2.addMonome(new Monomial(1,2));
        p2.addMonome(new Monomial(-1,0));
        assertTrue(model.dividePolynome(p1,p2).get(0).toString().equals("+1.0*X^1-2.0*X^0") &&
                   model.dividePolynome(p1,p2).get(1).toString().equals("+7.0*X^1-7.0*X^0"));
    }

}
